import 'dotenv/config';
import 'module-alias/register';
import validateEnv from './src/@core/util/validateEnv';
import { ProductController } from './src/module/product/product.controller';
import App from './src/server';

validateEnv();

const app = new App([
  new ProductController
], Number(process.env.PORT));

app.listen();