import { ProductRepository } from "../../@core/repository/product.repository";
import HttpException from "../../@core/util/exceptions/http.exception";
import { ProductService } from "./product.services";

jest.mock('../../@core/repository/product.repository.ts');

describe("ProductService", () => {
  const productRepository: {
    create: jest.Mock,
    getById: jest.Mock,
    getByName: jest.Mock,
    getAll: jest.Mock,
    update: jest.Mock,
    softDeleteById: jest.Mock,
  } = {} as any;

  beforeEach(() => {
    productRepository.create = jest.fn();
    ProductRepository.prototype.create = productRepository.create;

    productRepository.getById = jest.fn();
    ProductRepository.prototype.getById = productRepository.getById;

    productRepository.getByName = jest.fn();
    ProductRepository.prototype.getByName = productRepository.getByName;

    productRepository.getAll = jest.fn();
    ProductRepository.prototype.getAll = productRepository.getAll;
    
    productRepository.update = jest.fn();
    ProductRepository.prototype.update = productRepository.update;

    productRepository.softDeleteById = jest.fn();
    ProductRepository.prototype.softDeleteById = productRepository.softDeleteById;
  })

  afterEach(() => {
    jest.clearAllMocks();
  });
  
  describe("GET", () => {
    test('all product', async () => {
      const product = {
        "data": [
          {
            "isDelete": false,
            "_id": "6299ac56c6d4971888d43f76",
            "name": "1234",
            "quantity": 123,
            "price": 12,
            "created_at": "2022-06-03T06:38:14.444Z",
            "updated_at": "2022-06-08T02:54:30.002Z",
            "__v": 0
          },
          {
            "isDelete": false,
            "_id": "6299b084033ec337b4d0c6e0",
            "name": "123",
            "quantity": 123,
            "price": 12,
            "created_at": "2022-06-03T06:56:04.426Z",
            "updated_at": "2022-06-03T06:56:04.426Z",
            "__v": 0
          },
        ],
        "count": 2
      }
      const productService = new ProductService();
      productRepository.getAll.mockResolvedValue(product)
      const actual = await productService.getAllProduct();
      expect(actual).toEqual(product);
    })
    test('search product by name', async () => {
      const param = '123'
      const data = {
        name: '123',
        quantity: 100,
        price: 50,
      }
      const productService = new ProductService();
      productRepository.getByName.mockResolvedValue(data)
      const actual = await productService.searchProductByName(param);
      expect(actual).toEqual(data);
    });
    test('search product not found', async () => {
      const param = '123'
      const data = {
        name: '123',
        quantity: 100,
        price: 50,
      }
      const notFound = new HttpException(404, `ProductName=[${param}] not found.`);
      const productService = new ProductService();
      productRepository.getByName.mockRejectedValue(data)
      try {
        await productService.searchProductByName(param);
      } catch (error) {
        expect(error).toEqual(notFound);
      }
    });
  })
  describe("POST", () => {
    test('create', async () => {
      const data = {
        name: 'nikeADK',
        quantity: 100,
        price: 50,
      }
      const productService = new ProductService();
      productRepository.create.mockResolvedValue(data)
      const actual = await productService.create(data);
      expect(actual).toEqual(data);
    })
    test('cant create', async () => {
      const data = {
        name: 'nikeADK',
        quantity: 100,
        price: 50,
      }
      const notCreate = new HttpException(400, 'Unable to create product');
      const productService = new ProductService();
      productRepository.create.mockRejectedValue(notCreate)
      try {
        await productService.create(data);
      } catch (error) {
        expect(error).toEqual(notCreate);
      }
    });
  })
  describe("PUT", () => {
    test('update product', async () => {
      const param = '6299b084033ec337b4d0c6e0'
      const data = {
        name: 'nikeADK',
        quantity: 100,
        price: 50,
      }
      const productService = new ProductService();
      productRepository.update.mockResolvedValue(data)
      const actual = await productService.updateProduct(param,data);
      expect(actual).toEqual(data);
    })
    test('cant update product', async () => {
      const param = '6299b084033ec337b4d0c6e0'
      const data = {
        name: 'nikeADK',
        quantity: 100,
        price: 50,
      }
      const notUpdate = new HttpException(400, `ProductId=[${param}] can't update.`);
      const productService = new ProductService();
      productRepository.update.mockRejectedValue(notUpdate)
      try {
        await productService.updateProduct(param,data);
      } catch (error) {
        expect(error).toEqual(notUpdate);
      }
    });
  })
  describe("DELETE", () => {
    test('delete product', async () => {
      const param = '6299b084033ec337b4d0c6e0'
      const data = {
        name: 'nikeADK',
        quantity: 100,
        price: 50,
        isDelete: true
      }
      const productService = new ProductService();
      productRepository.softDeleteById.mockResolvedValue(data)
      const actual = await productService.removeProduct(param);
      expect(actual).toEqual(data);
    })
    test('cant delete product', async () => {
      const param = '6299b084033ec337b4d0c6e0'
      const notDelete = new HttpException(400, `ProductId=[${param}] can't remove.`);
      const productService = new ProductService();
      productRepository.softDeleteById.mockRejectedValue(notDelete)
      try {
        await productService.removeProduct(param);
      } catch (error) {
        expect(error).toEqual(notDelete);
      }
    });
  })
})