import { ProductRepository } from "../../@core/repository/product.repository";
import { IAllProduct, IProduct } from "../../@core/schema/product.schema";
import HttpException from "../../@core/util/exceptions/http.exception";

export class ProductService {
  private productRepository: ProductRepository;

  constructor(){
    this.productRepository = new ProductRepository();
  }

  public async create(product: IProduct): Promise<IProduct> {
    try {
      const data = await this.productRepository.create(product)
      return data;
    } catch (error) {
      throw new HttpException(400, 'Unable to create product');
    }
  }

  public async searchProduct(id: string): Promise<IProduct> {
    try {
      const data = await this.productRepository.getById(id)
      return data;
    } catch (error) {
      throw new HttpException(404, `ProductId=[${id}] not found.`);
    }
  }

  public async searchProductByName(name: string): Promise<IProduct> {
    try {
      const data = await this.productRepository.getByName(name)
      return data;
    } catch (error) {
      throw new HttpException(404, `ProductName=[${name}] not found.`);
    }
  }

  public async getAllProduct(): Promise<IAllProduct> {
    const data = await this.productRepository.getAll();
    return data;
  }

  public async removeProduct(id: string): Promise<IProduct> {
    try {
      const data = await this.productRepository.softDeleteById(id);
      return data;
    } catch (error) {
      throw new HttpException(400, `ProductId=[${id}] can't remove.`);
    }
  }

  public async updateProduct(id: string, product: IProduct): Promise<IProduct> {
    try {
      const data = await this.productRepository.update(id, product);
      return data;
    } catch (error) {
      throw new HttpException(400, `ProductId=[${id}] can't update.`);
    }
  }
}

