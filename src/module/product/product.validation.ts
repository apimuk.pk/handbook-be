import Joi from "joi";

const product = Joi.object({
  name: Joi.string().required(),
  quantity: Joi.number().required(),
  price: Joi.number().required(),
});

export default { product };