import { NextFunction, Request, Response, Router } from "express";
import validationMiddleware from '../../@core/middleware/validation.middleware';
import Controller from "../../@core/util/interfaces/controller.interface";
import { ProductService } from "./product.services";
import validate from "./product.validation";

export class ProductController implements Controller {
  public path = '/warehouse';
  public router = Router();
  private productService: ProductService;

  constructor() {
    this.productService = new ProductService();
    this.initialiseRoutes();
  }

  public initialiseRoutes(): void {
    this.router.post('/product/', [validationMiddleware(validate.product)], (req: any, res: any, next: any) => this.create(req, res, next));
    this.router.get('/product/', [], (req: any, res: any, next: any) => this.showAllProduct(req, res, next));
    this.router.get('/product/:name', [], (req: any, res: any, next: any) => this.searchProduct(req, res, next));
    this.router.delete('/product/:id', [], (req: any, res: any, next: any) => this.removeProduct(req, res, next));
    this.router.put('/product/:id', [validationMiddleware(validate.product)], (req: any, res: any, next: any) => this.updateProduct(req, res, next));
  }

  public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = {
        name: req.body.name,
        quantity: req.body.quantity,
        price: req.body.price
      }
      const product = await this.productService.create(data);
      res.status(201).json({ product })
    } catch (error) {
      next(error)
    }
  }

  public async searchProduct(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const name = req.params.name
      const product = await this.productService.searchProductByName(name);
      res.status(200).json({ product })
    } catch (error) {
      next(error)
    }
  }

  public async showAllProduct(
    req: Request,
    res: Response,
    next: NextFunction) {
    try {
      const product = await this.productService.getAllProduct();
      res.status(200).json(product)
    } catch (error) {
      console.error(error);
      next(error)
    }
  }

  public async removeProduct(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const product = await this.productService.removeProduct(req.params.id)
      res.status(200).json(product)
    } catch (error) {
      next(error)
    }
  }

  public async updateProduct(req: Request, res: Response, next: NextFunction): Promise<void> {
    const id = req.params.id
    const product = {
      name: req.body.name,
      quantity: req.body.quantity,
      price: req.body.price,
    }
    try {
      const data = await this.productService.updateProduct(id, product)
      res.status(200).json(data)
    } catch (error) {
      next(error)
    }
  }
}

