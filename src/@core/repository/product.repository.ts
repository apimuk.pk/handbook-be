import { Model, Types } from "mongoose";
import { IAllProduct, IProduct, IProductDocument, Product } from "../../@core/schema/product.schema";
import HttpException from "../util/exceptions/http.exception";

let productModel: any;
export class ProductRepository {
  private productRepository: Model<IProductDocument>

  constructor(model:any = undefined) {
    productModel = model || productModel || new Product().model;
    this.productRepository = productModel;
  }

  public async create(product: IProduct): Promise<IProductDocument> {
    const data = await this.productRepository.create(product);
    return data;
  }

  public async getById(id: string): Promise<IProductDocument> {
    const data = await this.productRepository.findOne({ _id: id });
    if (!data) throw new HttpException(404, `ProductId=[${id}] not found.`);
    return data;
  }

  public async getByName(name: string): Promise<IProductDocument> {
    const data = await this.productRepository.findOne({ name: name });
    if (!data) throw new HttpException(404, `ProductName=[${name}] not found.`);
    return data;
  }

  public async getAll(): Promise<IAllProduct> {
    const data: IProductDocument[] = await this.productRepository.find({ isDelete: false });
    if (!data) throw new HttpException(404, `Product not found.`);
    const response = {
      data,
      count: data.length
    }
    return response;
  }

  public async softDeleteById(id: string): Promise<IProduct> {
    const data = await this.productRepository.findOneAndUpdate({ _id: id }, { isDelete: true });
    if (!data) throw new HttpException(404, `ProductId=[${id}] not found.`);
    return data;
  }

  public async update(id: string, product: IProduct): Promise<IProductDocument> {
    const data = await this.productRepository.findOneAndUpdate({ _id: Types.ObjectId(id) }, product, { new: true });
    if (!data) throw new HttpException(404, `ProductId=[${id}] not found.`);
    return data;
  }
}