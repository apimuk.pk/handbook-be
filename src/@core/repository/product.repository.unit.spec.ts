import HttpException from "../util/exceptions/http.exception";
import { ProductRepository } from "./product.repository";
class Mock {
    create: () => {}
    find: () => {}
    findOne: () => {}
    findOneAndUpdate: () => {}

    get model() {
        return {
            create: this.create,
            find: this.find,
            findOne: this.findOne,
            findOneAndUpdate: this.findOneAndUpdate,
        }
    }
}

describe("productRepository", () => {
    describe("GET", () => {
        test('all product', async () => {
            const product = {
                "0": { name: "nikeABC", price: 150, quantity: 50 }, 
                "1": { name: "nikeXYZ", price: 200, quantity: 100 }
            }
            const mock = new Mock();
            mock.find = jest.fn().mockResolvedValue({ ...product })
            const repository = new ProductRepository(mock);
            const actual = await repository.getAll();
            expect(actual.data).toEqual(product)
        })
        test('all product not found', async () => {
            const notFound = new HttpException(404, 'Product not found.');
            const mock = new Mock();
            mock.find = jest.fn().mockRejectedValue(notFound)
            const repository = new ProductRepository(mock);
            try {
                await repository.getAll();
            } catch (error) {
                expect(error).toEqual(notFound)
            }
        })
        test('search product', async () => {
            const param = 'nikeXYZ';
            const product = {
                name: 'nikeXYZ',
                quantity: 100,
                price: 200
            }
            const mock = new Mock();
            mock.findOne = jest.fn().mockResolvedValue({ ...product })
            const repository = new ProductRepository(mock);
            const actual = await repository.getByName(param);
            expect(actual.name).toEqual(product.name)
        })
        test('search product not found', async () => {
            const param = 'nikeABC'
            const notFound = new HttpException(404, `ProductName=[${param}] not found.`);
            const mock = new Mock();
            mock.findOne = jest.fn().mockRejectedValue(notFound)
            const repository = new ProductRepository(mock);
            try {
                await repository.getByName(param);
            } catch (error) {
                expect(error).toEqual(notFound)
            }
        })
    })

    describe("POST", () => {
        test('create product', async () => {
            const product = {
                name: 'nikeXYZ',
                quantity: 100,
                price: 200
            }
            const mock = new Mock();
            mock.create = jest.fn().mockResolvedValue({ ...product })
            const repository = new ProductRepository(mock);
            const actual = await repository.create(product);
            expect(actual.name).toEqual(product.name)
        })
    })
    describe("PUT", () => {
        test('update product', async () => {
            const param = '6299b084033ec337b4d0c6e0';
            const product = {
                name: 'nikeDEF',
                quantity: 100,
                price: 200
            }
            const mock = new Mock();
            mock.findOneAndUpdate = jest.fn().mockResolvedValue({ ...product })
            const repository = new ProductRepository(mock);
            const actual = await repository.update(param,product);
            expect(actual).toEqual(product)
        })
        test('cant update product', async () => {
            const param = '6299b084033ec337b4d0c6e0';
            const product = {
                name: 'nikeDEF',
                quantity: 100,
                price: 200
            }
            const notFound = new HttpException(404, `ProductId=[${param}] not found.`);
            const mock = new Mock();
            mock.findOneAndUpdate = jest.fn().mockRejectedValue(notFound)
            const repository = new ProductRepository(mock);
            try {
                await repository.update(param,product);
            } catch (error) {
                expect(error).toEqual(notFound)
            }
        })
    })
    describe("DELETE", () => {
        test('remove product', async () => {
            const param = '6299b084033ec337b4d0c6e0';
            const product = {
                name: 'nikeXYZ',
                quantity: 100,
                price: 200,
                isDelete: true,
            }
            const mock = new Mock();
            mock.findOneAndUpdate = jest.fn().mockResolvedValue({ ...product })
            const repository = new ProductRepository(mock);
            const actual = await repository.softDeleteById(param);
            expect(actual).toEqual(product)
        })
        test('cant remove product', async () => {
            const param = '6299b084033ec337b4d0c6e0';
            const mock = new Mock();
            const notFound = new HttpException(404, `ProductId=[${param}] not found.`);
            mock.findOneAndUpdate = jest.fn().mockRejectedValue(notFound)
            const repository = new ProductRepository(mock);
            try {
                await repository.softDeleteById(param);
            } catch (error) {
                expect(error).toEqual(notFound)
            }
        })
    })

})