import { Connection, createConnection, Document, Model, Schema, Types } from 'mongoose';

export interface IProduct {
  _id?: Types.ObjectId;
  name: string;
  quantity: number;
  price: number,
  isDelete?: boolean,
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IAllProduct {
  data: IProduct[]
  count: number,
}

export interface IProductDocument extends IProduct, Document {  _id?: Types.ObjectId; }

export interface ProductModel extends Model<IProductDocument> { };

export class Product {

  private _model: Model<IProductDocument>;
  private _connection: Connection;

  constructor() {
    const mongoDB = {
      host: process.env.MONGODB_HOST || 'localhost',
      port: process.env.MONGODB_PORT || '27018',
      db: process.env.MONGODB_DATABASE_NAME || 'warehouse',
      options: process.env.MONGODB_OPTIONS || ''
    }
    const schema = new Schema({
      name: { type: String, required: true, indexes: true },
      quantity: { type: Number, required: true },
      price: { type: Number, required: true },
      isDelete: { type: Boolean, default: false },
    }, {
      timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
      }
    });
    const url = `mongodb://${mongoDB.host}:${mongoDB.port}/${mongoDB.db}${mongoDB.options}`;
    this._connection = createConnection(url, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true });
    console.log('==> productModel connection to:', url);
    this._connection.on('open', () => console.log('Mongoose has connected'));
    this._connection.on('error', (err) => {
      console.log('Mongoose has errored', err);
      process.exit(1);
    });
    this._model = this._connection.model<IProductDocument>('Product', schema);
  }

  public get model(): Model<IProductDocument> {
    return this._model
  }
}