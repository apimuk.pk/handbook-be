import compression from 'compression';
import cors from 'cors';
import express, { Application } from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import ErrorMiddleware from './@core/middleware/error.middleware';
import Controller from './@core/util/interfaces/controller.interface';

class App {
  public express: Application;
  public port: number;
  private allowOrigin: string[] = ["http://localhost:4200"]
  private options: cors.CorsOptions;

  constructor(controllers: Controller[], port: number) {
    this.express = express()
    this.port = port

    this.initialiseMiddleware()
    this.initialiseControllers(controllers)
    this.initialiseErrorHandling()
  }

  private initialiseMiddleware(): void{
    this.express.use(helmet());
    this.options = { origin: this.allowOrigin }
    this.express.use(cors(this.options));
    this.express.use(morgan('dev'));
    this.express.use(express.json());
    this.express.use(express.urlencoded({extended:false}));
    this.express.use(compression());
  }

  private initialiseControllers(controllers: Controller[]): void {
    controllers.forEach((controller: Controller)=>{
      this.express.use(`${controller.path}`, controller.router)
    })
  }

  private initialiseErrorHandling(): void {
    this.express.use(ErrorMiddleware);
  }

  public listen(): void {
    this.express.listen(this.port, ()=> {
      console.log(`App listening on port ${this.port}`)
    })
  }
}

export default App;